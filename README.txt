This module provides a block with a linked list of all the URLs mentioned in the
node body.  To use, simply install and enable the "Referenced URLs Block".
